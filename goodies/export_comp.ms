-- Indexes for output
obb_index = 0
elem_index = 0
element_array = #()

mapped fn export_comp_func node tabs filep path lodpath geo_list_file mesh_extension =
(
	-- Format string
	tabstr = ""
	for i = 1 to tabs do
		tabstr += "\t"
		
	-- If not a group
	if ((findstring node.name "Group") == undefined) then
	(
		-- Element
		if ((findstring node.name "Box") == undefined) do
		(
			-- Center pivot to object
			node.pivot = node.center

			-- Format
			format "\n% %\n" (tabstr+"ELEMENT") elem_index to: filep
			format "%\n" (tabstr+node.name) to: filep
			format "%\n" (tabstr+node.material.name) to: filep
			format "%\n" (tabstr+"STATIC") to: filep
			format "%% % %\n" tabstr node.pos.x node.pos.y node.pos.z to: filep
			format "%\n" (tabstr+"EULER") to: filep
			format "%% % %\n" tabstr node.rotation.x_rotation node.rotation.y_rotation node.rotation.z_rotation to: filep
			
			-- Create base LOD spec file
			-- Open file
			lodfile = createFile (path+"/"+node.name+".lod")
			
			-- Export info
			format "%\n" node.name to: lodfile
			format "1\n" to: lodfile
			format "CANCEL_ROTATION\n" to: lodfile
			format "% % %\n\n" node.rotation.x_rotation node.rotation.y_rotation node.rotation.z_rotation to: lodfile
			format "%\n" (lodpath+"/"+node.name+"."+mesh_extension) to: lodfile
			
			-- Close file
			close lodfile
			
			-- Add this entry to geometry list file
			format "%\n" (lodpath+"/"+node.name+".lod") to: geo_list_file
			
			-- Append element to element array
			append element_array node
				
			-- Update element index
			elem_index += 1
		)
	)

	-- If group
	else
	(
		-- Find group bounding box
		for child in node do
		(
			-- Bounding box
			if((findString child.name ("Box"+(substring node.name 6 -1))) != undefined) do
			(
				-- Center pivot to object
				child.pivot = child.center
				
				-- Format
				format "\n% %\n" (tabstr+"OBB") obb_index to: filep
				format "%\n" (tabstr+"STATIC") to: filep
				format "%% % %\n" tabstr child.width child.length child.height to: filep
				format "%% % %\n" tabstr child.pos.x child.pos.y child.pos.z to: filep
				format "%\n" (tabstr+"EULER") to: filep
				format "%% % %\n" tabstr child.rotation.x_rotation child.rotation.y_rotation child.rotation.z_rotation to: filep
				format "\n%\n" (tabstr+"OPEN") to: filep
				
				-- Update OBB index
				obb_index += 1
			)
		)
		
		-- Recursive call
		export_comp_func node.children (tabs+1) filep path lodpath geo_list_file mesh_extension

		-- Format
		format "\n%\n" (tabstr+"CLOSE") to: filep
	)
)


utility export_comp "Export composite"
(
	-- Caption
	label inst1 "Enter topmost"
	label inst2 "group before exporting"

	-- Text box for entering composite ID
	edittext compid_box "ID:" fieldWidth: 50 text: "1"
	
	-- Text box for entering topmost group name
	edittext grpname_box "Group:" fieldWidth: 80 text: "Group01"
	
	-- Checkbox for centering elements at origin
	checkbox elem_center_check "Center elements" checked: false
	
	-- Separation label
	label line0

	-- Text box for relative path inside LOD files
	edittext lodpath_box "LODPath:" fieldWidth: 95 text: "models"
	
	-- Text box for mesh extension
	edittext mesh_ext_box "Mesh extension:" fieldWidth: 40 text: "ase"

	-- Separation label
	label line1

	-- Text box for entering path
	edittext path_box "Path:" fieldWidth: 110 text: "c:/temp"
	
	-- Text box for entering filename
	edittext filename_box "File:" fieldWidth: 110 text: "comp1.comp"
	
	-- Separation label
	label line2

	-- ASE list creation
	checkbox ase_list_check "Create ASE list" checked: false
	edittext ase_list_box "ASEList:" fieldWidth: 100 text: "ase_list.txt" enabled: false
	on ase_list_check changed true do
		ase_list_box.enabled = true
	
	-- Button for exporting action
	button export_button "Export"

	-- If the export button is pressed, export composite
	on export_button pressed do
	(
		local top_group
		local root_bb
				
		-- Open file
		file = createFile (path_box.text+"/"+filename_box.text)

		-- Ok, start with given group
		for groups in $Group* do
			if (groups.name == grpname_box.text) do top_group = groups

		-- Get number of bounding boxes and elements
		num_bb = -1 -- Don't count root BB
		num_elem = 0
		for node in objects do
		(
			if ((findString node.name "Box") != undefined) then
				num_bb += 1
				
			else
				if ((findString node.name "Group") == undefined) do
					num_elem += 1
		)
		
		-- Get root BB
		for node in top_group.children do
			if ((findString node.name "Box") != undefined) do
			(
				root_bb = node
				exit
			)
		
		-- Export root first
		format "\n%\n" (compid_box.text as integer) to: file
		format "% % %\n" root_bb.width root_bb.length root_bb.height to: file
		format "% %\n\n" num_bb num_elem to: file
		format "\nOPEN\n" to:file
		
		-- Set indexes
		obb_index = num_elem
		elem_index = 0
		element_array = #()
		
		-- Open geometry list file
		geo_list_file = createFile (path_box.text+"/"+"geometries.col")
		
		-- Export remaining nodes
		export_comp_func top_group.children 1 file path_box.text lodpath_box.text geo_list_file mesh_ext_box.text
		
		format "\nCLOSE\n" to: file
		
		-- Close files
		close file
		close geo_list_file
		
		-- Export elements
		-- Create ASE list file?
		if(ase_list_check.state == true) then
			ase_list_file = createFile (path_box.text+"/"+ase_list_box.text)
			
		explodeGroup top_group
		for node in element_array do
		(
			select node
			original_center = node.center
			node.center.x = node.center.y = node.center.z = 0.0
			exportFile (path_box.text+"/"+node.name+".ase") #noPrompt selectedOnly: true
			
			if(ase_list_check.state == true) then
				format "%\n" (lodpath_box.text+"/"+node.name+".ase") to: ase_list_file
			
			if(elem_center_check.state != true) then
				node.center = original_center
		)
		
		-- Close ASE list file?
		if(ase_list_check.state == true) then
			close ase_list_file
		
		-- Success message
		messageBox "File exported with success"
	)
)