// Simple normal-mapped per-pixel lighting

// Per-pixel input
varying vec3 IHalf;
varying vec3 ILight;
varying vec2 TexCoord;

// Uniforms
uniform sampler2D diffuse_map;
uniform sampler2D gloss_map;
uniform sampler2D emissive_map;
uniform sampler2D normal_map;

uniform vec4 mat_d;
uniform vec4 mat_s;
uniform float mat_sh;


void main(void)
{
	vec3 N = normalize(((texture2D(normal_map, TexCoord) - 0.5) * 2.0).rgb);
	vec3 L = (ILight);

	// Diffuse term
	float d_term = max(dot(N, L), 0.0);

	// Initial diffuse color
	vec4 smoke_color = texture2D(diffuse_map, TexCoord);
	vec4 color = vec4(gl_Color.rgb * d_term * (1.0 - smoke_color.rgb), gl_Color.a * smoke_color.a);
//	vec4 color = mat_d * texture2D(diffuse_map, TexCoord).a * d_term;
//	vec4 color = mat_d * d_term;

	// Specular term
	vec3 H = (IHalf);
//	float gloss = texture2D(gloss_map, TexCoord).a;
	float shininess = mat_sh;
//	float s_term = pow(max(dot(N, H), 0.0), shininess * 4.0) * gloss;
//	float s_term = pow(max(dot(N, H), 0.0), shininess * 4.0);

	// Specular component
//	color += s_term * mat_s;

	// Emissive component
//	color += texture2D(emissive_map, TexCoord);

	gl_FragColor = color;
}

