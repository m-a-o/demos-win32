// Simple normal-mapped per-pixel lighting

// Per-pixel output
varying vec3 IHalf;
varying vec3 ILight;
varying vec2 TexCoord;

// Uniforms
uniform vec3 eye_os;
uniform vec3 obj_ws;
uniform mat4 model_matrix;
uniform vec4 light_dir;


void main(void)
{
	gl_Position = ftransform();

	// Light
	vec3 light_ws = light_dir.xyz;
	vec3 light_os = (vec4(light_ws, 1.0) * model_matrix).xyz;
	vec3 L = normalize(light_os);

	// Half vector
	vec3 P = gl_Vertex.xyz;
	vec3 N = gl_Normal;

	vec3 V = normalize(eye_os - P);
	vec3 H = normalize(L + V);

	// Output
	IHalf = H;
	ILight = L;
	TexCoord = gl_MultiTexCoord0.st;
	gl_FrontColor = gl_Color;
}
