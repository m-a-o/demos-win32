varying vec4 the_color;

uniform vec3 uniform_color;

void main(void) {

	the_color = vec4(uniform_color, 1.0);

	gl_Position = ftransform();

	return;

	}
