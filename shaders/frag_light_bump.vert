
varying vec3 IHalf;
varying vec3 ILight0;
varying vec3 ILight1;
varying vec2 TexCoord;

uniform vec3 eye_os;
uniform vec3 obj_ws;
uniform mat4 model_matrix;


void main(void)
{
	gl_Position = ftransform();

	// Light 0
	vec3 light_ws = vec3(417.337, 225.558, -201.55);
	vec3 light_os = (vec4(light_ws, 1.0) * model_matrix).xyz;
	vec3 L0 = normalize(light_os);

	// Light 1
	light_ws = vec3(0.0, 0.0, 1.0);
	light_os = (vec4(light_ws, 1.0) * model_matrix).xyz;
	vec3 L1 = normalize(light_os);

	vec3 P = gl_Vertex.xyz;
	vec3 N = gl_Normal;

	vec3 V = normalize(eye_os - P);
	vec3 H = normalize(L0 + V);

	IHalf = H;
	ILight0 = L0;
	ILight1 = L1;
	TexCoord = gl_MultiTexCoord0.st;

	return;
}

