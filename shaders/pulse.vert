#define OFFSET_SIZE 1.5

varying vec3 INormal;
varying vec3 IHalf;
varying vec3 ILight;
varying vec2 TexCoord;
varying vec3 IEye;

uniform vec3 eye_os;
uniform vec3 obj_ws;
uniform mat4 model_matrix;
uniform vec3 eye_ws;
uniform float time;

void main(void)
{
	// Perturb vertex
	vec4 P_offset = gl_Vertex;
	float offset = sin(radians(time * 240.0)) * OFFSET_SIZE;
	P_offset.xyz += gl_Normal * abs(offset);

	gl_Position = (gl_ModelViewProjectionMatrix * P_offset);

	// Light
	vec3 light_ws = vec3(1.0, 1.0, 1.0);
	vec3 light_os = (vec4(light_ws, 1.0) * model_matrix).xyz;
	vec3 L = normalize(light_os);

	vec3 P = P_offset.xyz;
	vec3 N = gl_Normal;

	vec3 V = normalize(eye_os - P);
	vec3 H = (L + V);

	INormal = N;
	IHalf = H;
	ILight = L;
	TexCoord = gl_MultiTexCoord0.st;
	IEye = V;

	return;
}

