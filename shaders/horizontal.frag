// No trails
//#define T_DIM 128.0
// Glow trails
#define T_DIM 1024.0
#define U_OFFSET (1.0 / T_DIM)

uniform sampler2D diffuse_map;


void main(void)
{
	vec2 off_1 = vec2(U_OFFSET, 0.0);
	vec2 off_2 = vec2(U_OFFSET * 2.0, 0.0);
	vec2 off_3 = vec2(U_OFFSET * 3.0, 0.0);
	vec2 off_4 = vec2(U_OFFSET * 4.0, 0.0);

	vec4 color = texture2D(diffuse_map, gl_TexCoord[0].st + off_1) * 35.0;
	color += texture2D(diffuse_map, gl_TexCoord[0].st + off_2) * 21.0;
	color += texture2D(diffuse_map, gl_TexCoord[0].st + off_3) * 7.0;
	color += texture2D(diffuse_map, gl_TexCoord[0].st + off_4);

	color += texture2D(diffuse_map, gl_TexCoord[0].st - off_1) * 35.0;
	color += texture2D(diffuse_map, gl_TexCoord[0].st - off_2) * 21.0;
	color += texture2D(diffuse_map, gl_TexCoord[0].st - off_3) * 7.0;
	color += texture2D(diffuse_map, gl_TexCoord[0].st - off_4);

	color /= 128.0;

	gl_FragColor = color;

	return;
}
