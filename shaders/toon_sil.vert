// Toon shading - silhouette rendering

//#define SILHOUETTE_OFFSET 0.2
uniform float sil_offset;




void main(void)
{
	float SILHOUETTE_OFFSET = sil_offset * 0.01;
	vec4 p_offset = vec4(gl_Vertex.xyz + (gl_Normal * SILHOUETTE_OFFSET), 1.0);
	gl_Position = gl_ModelViewProjectionMatrix * p_offset;

	gl_FrontColor = vec4(0.0, 0.0, 0.0, 1.0);

	return;
}

