
varying vec3 INormal;
varying vec3 IHalf;
varying vec3 ILight;
varying vec2 TexCoord;

uniform sampler2D diffuse_map;
uniform sampler2D gloss_map;
uniform sampler2D emissive_map;
uniform vec4 mat_d;
uniform vec4 mat_s;
uniform float mat_sh;

void main(void)
{
	vec3 N = normalize(INormal);
//	float3 N = normalize(((tex2D(emissive_map, input.TexCoord) - 0.5) * 2.0).xyz);
	vec3 L = normalize(ILight);

	// Diffuse term
	float d_term = max(dot(N, L), 0.0);

	// Initial diffuse color
	vec4 color = mat_d * texture2D(diffuse_map, TexCoord) * d_term;
//	float4 color = mat_d * d_term;

	// Specular term
	vec3 H = normalize(IHalf);
	float gloss = texture2D(gloss_map, TexCoord).a;
	float shininess = mat_sh;
	float s_term = pow(max(dot(N, H), 0.0), shininess * 4.0) * gloss;
//	float s_term = pow(max(dot(N, H), 0.0), shininess * 4.0);

	// Specular component
	color += s_term * mat_s;

	// Emissive component
	color += texture2D(emissive_map, TexCoord);

	gl_FragColor = color;

	return;
}

