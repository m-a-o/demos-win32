// Toon shading - diffuse shading (two-tone)

// Per-pixel output
varying vec2 TexCoord;

// Uniforms
uniform mat4 model_matrix;




void main(void)
{
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;

	// Diffuse shading
	vec3 light_ws = normalize(vec3(1.0, 1.0, 1.0));
	vec3 light_os = (vec4(light_ws, 1.0) * model_matrix).xyz;

	float d_term = max(dot(gl_Normal, light_os), 0.0);

	TexCoord = vec2(d_term, 0.0);

	return;
}

