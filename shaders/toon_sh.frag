// Toon shading - diffuse shading (two-tone)

// Per-pixel input
varying vec2 TexCoord;

// Uniforms
uniform vec4 mat_d;
uniform sampler2D diffuse_map;




void main(void)
{
	gl_FragColor = mat_d * texture2D(diffuse_map, TexCoord);

	return;
}
