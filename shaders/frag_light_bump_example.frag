
varying vec3 IHalf;
varying vec3 ILight;
varying vec2 TexCoord;

uniform sampler2D diffuse_map;
uniform sampler2D gloss_map;
uniform sampler2D emissive_map;
uniform sampler2D normal_map;
uniform vec4 mat_d;
uniform vec4 mat_s;
uniform float mat_sh;


void main(void)
{
	vec3 N = normalize(((texture2D(normal_map, TexCoord) - 0.5) * 2.0).xyz);
	vec3 L = (ILight);

	// Diffuse term
	float d_term = max(dot(N, L), 0.0);

	// Initial diffuse color
	gl_FragColor =  mat_d * d_term;

	return;
}

