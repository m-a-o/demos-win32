// Simple grass animation

uniform mat4 model_matrix;


void main(void)
{
	if(gl_MultiTexCoord0.y > 0.0) {
	
		vec3 offset = (gl_Color.xyz * gl_Color.w) * mat3(model_matrix);
		gl_Position = gl_Vertex + vec4(offset, 0.0);
		gl_Position = gl_ModelViewProjectionMatrix * gl_Position;

		}

	else
		gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;

	// Pass-through
	gl_TexCoord[0] = gl_MultiTexCoord0;
	gl_FrontColor = vec4(1.0);

	return;
}
