// Heat shimmer effect

#define MAX_OFFSET 0.01


// Uniforms
uniform sampler2D fb_texture;
uniform sampler2D heat_mask;
uniform sampler2D heat_noise;


void main(void)
{
	vec3 noise = texture2D(heat_noise, gl_TexCoord[2]);
	vec3 mask = texture2D(heat_mask, gl_TexCoord[1]);

	float max_offset = gl_FrontMaterial.shininess * 0.0001;

	// Horizontal displacement on red component
	float h_offset = gl_TexCoord[0].s + (mask.r * max_offset * ((noise.r - 0.5) * 2.0));

	// Vertical displacement is green component
	float v_offset = gl_TexCoord[0].t + (mask.r * max_offset * ((noise.g - 0.5) * 2.0));

	// Final color
	gl_FragColor = texture2D(fb_texture, vec2(h_offset, v_offset));
//	gl_FragColor = texture2D(heat_mask, gl_TexCoord[1]);
}

