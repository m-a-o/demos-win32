// Reflection / refraction shader

// Per-pixel input
varying vec3 INormal;
varying vec3 IHalf;
varying vec3 ILight;
varying vec2 TexCoord;
varying vec3 IEye;

// Uniforms
uniform sampler2D diffuse_map;
uniform sampler2D gloss_map;
uniform samplerCube cube_map;
uniform samplerCube rfl_cube_map;
uniform vec4 mat_d;
uniform vec4 mat_s;
uniform float mat_sh;
uniform mat4 model_matrix;


// Auxiliary function
vec3 refract(vec3 I, vec3 N, float etaRatio)
{
	float cosI = dot(-I, N);
	float cosT2 = 1.0 - etaRatio * etaRatio * (1.0 - cosI * cosI);

	vec3 T = etaRatio * I + ((etaRatio * cosI - sqrt(abs(cosT2))) * N);

	if(cosT2 > 0.0)
		return T;

	else
		return vec3(0.0, 0.0, 0.0);
}


void main(void)
{
	vec3 N = normalize(INormal);
	vec3 L = normalize(ILight);
	vec3 H = normalize(IHalf);

	// Refraction / reflection vector
	vec3 Rfr = refract(-IEye, N, (1.0 / 1.3));
//	Rfr = mul((float3x3)model_matrix_frag, Rfr);
	Rfr = (model_matrix * vec4(Rfr, 1.0)).xyz;
	vec3 Rfl = reflect(-IEye, N);
//	Rfl = mul((float3x3)model_matrix_frag, Rfl);
	Rfl = (model_matrix * vec4(Rfl, 1.0)).xyz;

	// Specular term
	float s_term = pow(max(dot(N, H), 0.0), mat_sh * 4.0);

	// Specular color
//	float gloss = tex2D(gloss_map, input.TexCoord).w;
	vec4 color = s_term * mat_s;
	vec4 color_refract = textureCube(cube_map, Rfr);
	vec4 color_reflect = textureCube(rfl_cube_map, Rfl);

	color += mix(color_refract, color_reflect, 0.02);

	gl_FragColor = color;
}

